# 「7.2.4 GitOpsの実装」のサンプルアプリケーション

7章 GitOps（CD）の実装で利用するサンプルアプリケーションのソースコードとGitLab CIのサンプル実装です。

## CIパイプライン

7章で説明するCIパイプラインの全体を実装しています。

### パイプラインの内容

1. stage-config-scan
   * Dockerfileの設定ミス、脆弱性の混入などのスキャンを実施する
1. stage-lint
   * アプリケーションコードの静的チェックを実施する
1. stage-test
   * アプリケーションコードのテストを実施する
1. stage-build
   * コンテナイメージのビルドを実施する
1. stage-image-scan
   * コンテナイメージの脆弱性スキャンを実施する
1. stage-push
   * コンテナイメージをコンテナレジストリへプッシュする
1. stage-tag-image
   * コンテナイメージにタグを付与し、リリースする
1. stage-gitlab-release
   * GitLab リリースを作成する

## アプリケーションの処理内容

アプリケーションを起動し、 `http://{hostname}/` へアクセスすると `hello world` をレスポンスする。

## アプリケーションのビルド・実行方法

### goコマンドを実行する方法

以下のコマンドを実行し、ビルドする。

```bash
$ CGO_ENABLED=0 go build -ldflags '-s -w' -trimpath -o app -a main.go
// omit
```

以下のコマンドを実行し、アプリケーションを起動する。

```bash
$ ./app
// omit
```

`http://localhost:8080/` へアクセスすると、 `hellow world` をレスポンスする。

### dockerコマンドを実行する方法

以下のコマンドを実行し、ビルドする。

```bash
$ docker build -t {イメージ名}:{タグ名} .
// omit
```

例えば、下記のように実行する。

```bash
$ docker build -t gihyo/hello-world:latest .
// omit
```

以下のコマンドを実行し、アプリケーションを起動する。

```bash
$ docker run -d -p 8080:8080 --name hello-world gihyo/hello-world:latest
// omit
```

`http://localhost:8080/` へアクセスすると、 `hellow world` をレスポンスする。

以下のコマンドを実行し、アプリケーションを停止する。

```bash
$ docker stop hello-world
// omit
```

### アプリケーションのテスト実行方法

以下のコマンドを実行し、テストを実行する。

```bash
go test -v ./...
```
